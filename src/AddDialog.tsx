import Button from '@material-ui/core/Button';
import CircularProgress from "@material-ui/core/CircularProgress";
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import NotInterestedIcon from "@material-ui/icons/NotInterested"
import ThumbUpIcon from "@material-ui/icons/ThumbUp"
import React from 'react';
import {atom, RecoilState, useRecoilState, useSetRecoilState} from "recoil/dist";
import {cardsState} from "./Names";

export const addDialogOpenedState: RecoilState<boolean> = atom<boolean>({
  key: 'addDialogState',
  default: false
});

function AddDialog() {
  const [isSaving, setIsSaving] = React.useState(false);

  const [isOpen, setIsOpen] = useRecoilState(addDialogOpenedState);

  const setCards = useSetRecoilState(cardsState);

  const handleClose = () => {
    setIsOpen(false);
  };

  const addIcon = isSaving
    ? <CircularProgress size={20}/>
    : <ThumbUpIcon/>;

  const handleAdd = async () => {
    setIsSaving(true);
    const response = await fetch('https://api.scryfall.com/cards/random');

    if (response.status === 200) {
      const card = await response.json();

      setCards((oldCards) => {
        const newCards = [...oldCards, card];
        return newCards.sort();
      });
    }

    setIsSaving(false);
    handleClose();
  };

  return (
    <Dialog open={isOpen} onClose={handleClose}>
      <DialogTitle>Take a moment to consider</DialogTitle>
      <DialogContent>Are you really sure you want to fetch a new name?</DialogContent>
      <DialogActions>
        <Button onClick={handleClose} color="primary" startIcon={<NotInterestedIcon/>}>No...</Button>
        <Button onClick={handleAdd} color="primary" startIcon={addIcon}>Oh, yes!</Button>
      </DialogActions>
    </Dialog>
  );
}

export default AddDialog;
