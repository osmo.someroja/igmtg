import React from 'react';

interface TabPanelProps {
  children?: React.ReactNode;
  index: number;
  currentIndex: number;
}

function TabPanel(props: TabPanelProps) {
  const {children, index, currentIndex, ...other} = props;
  const isVisible = index === currentIndex;

  return (
    <div
      role="tabpanel"
      hidden={!isVisible}
      id={`tabpanel-${index}`}
      {...other}
    >
      {isVisible && children}
    </div>
  );
}

export default TabPanel;
