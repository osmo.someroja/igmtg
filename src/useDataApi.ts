import {useState, useEffect} from 'react';
import {RecoilState, useRecoilState} from "recoil/dist";

function useDataApi<T>(url: string, state: RecoilState<T>): [T, boolean, boolean] {
  const [data, setData] = useRecoilState(state);
  const [isLoading, setIsLoading] = useState(false);
  const [isError, setIsError] = useState(false);

  useEffect(() => {
    const fetchData = async () => {
      setIsError(false);
      setIsLoading(true);

      try {
        const response = await fetch(url);

        if (response.status === 200) {
          const result = await response.json();

          // Here we assume that the actual data is in property called "data".
          // That might not be the case for your API though!
          setData(result.data);
        } else {
          setIsError(true);
        }
      } catch (error) {
        setIsError(true);
      }

      setIsLoading(false);
    };

    fetchData();
  }, [url, state, setData]);

  return [data, isLoading, isError];
}

export default useDataApi;
