import {Paper, Table, TableBody, TableCell, TableContainer, TableRow, Typography} from "@material-ui/core";
import React from "react";
import {atom, RecoilState} from "recoil/dist";
import useDataApi from "./useDataApi";

export interface Card {
  name: string;
}

export const cardsState: RecoilState<Card[]> = atom<Card[]>({
  key: "cardsState",
  default: []
});

function Names() {
  const [cards, isLoading, isError] = useDataApi("https://api.scryfall.com/cards/search?q=bears", cardsState);

  return (
    <React.Fragment>
      <Typography variant="h1">Names</Typography>
      <Typography>isLoading = {isLoading.toString()}</Typography>
      <Typography>isError = {isError.toString()}</Typography>
      <TableContainer component={Paper}>
        <Table>
          <TableBody>
            {cards.map((card: Card) => (
              <TableRow key={card.name}>
                <TableCell>{card.name}</TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </React.Fragment>
  );
}

export default Names;
