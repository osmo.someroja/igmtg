import {
  AppBar,
  CssBaseline,
  Divider,
  Drawer,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  Tab,
  Tabs,
  Toolbar,
  Typography,
} from "@material-ui/core";
import {createStyles, makeStyles, Theme} from "@material-ui/core/styles";
import RestaurantIcon from '@material-ui/icons/Restaurant';
import SentimentSatisfiedAltIcon from "@material-ui/icons/SentimentSatisfiedAlt";
import SettingsInputSvideoIcon from "@material-ui/icons/SettingsInputSvideo";
import React from "react";
import {useSetRecoilState} from "recoil/dist";
import AddDialog, {addDialogOpenedState} from "./AddDialog";
import Names from "./Names";
import TabPanel from "./TabPanel";

const drawerWidth = 240;

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: "flex",
    },
    appBar: {
      width: `calc(100% - ${drawerWidth}px)`,
      marginLeft: drawerWidth,
    },
    drawer: {
      width: drawerWidth,
      flexShrink: 0,
    },
    drawerPaper: {
      width: drawerWidth,
    },
    toolbarOffset: theme.mixins.toolbar,
    tabsOffset: theme.mixins.toolbar,
    content: {
      flexGrow: 1,
      backgroundColor: theme.palette.background.default,
      padding: theme.spacing(3),
    },
  })
);

function App() {
  const classes = useStyles();

  const [tabIndex, setTabIndex] = React.useState(0);
  const setAddDialogOpenedState = useSetRecoilState(addDialogOpenedState);

  const handleTabChange = (event: React.ChangeEvent<{}>, tabIndex: number) => {
    setTabIndex(tabIndex);
  };

  const handleAdd = () => {
    setAddDialogOpenedState(true);
  };

  return (
    <div className={classes.root}>
      <CssBaseline/>
      <AppBar position="fixed" className={classes.appBar}>
        <Toolbar>
          <Typography variant="h6" noWrap>IGMTG</Typography>
        </Toolbar>
        <Toolbar>
          <Tabs value={tabIndex} onChange={handleTabChange}>
            <Tab icon={<SentimentSatisfiedAltIcon/>}/>
            <Tab icon={<SettingsInputSvideoIcon/>}/>
          </Tabs>
        </Toolbar>
      </AppBar>
      <Drawer
        className={classes.drawer}
        variant="permanent"
        classes={{
          paper: classes.drawerPaper,
        }}
        anchor="left"
      >
        <div className={classes.toolbarOffset}/>
        <div className={classes.tabsOffset}/>
        <Divider/>
        <List>
          <ListItem button key="Add" onClick={handleAdd}>
            <ListItemIcon><RestaurantIcon/></ListItemIcon>
            <ListItemText primary="Add"/>
          </ListItem>
          <AddDialog/>
        </List>
      </Drawer>
      <main className={classes.content}>
        <div className={classes.toolbarOffset}/>
        <div className={classes.tabsOffset}/>
        <TabPanel index={0} currentIndex={tabIndex}>
          <Names/>
        </TabPanel>
        <TabPanel index={1} currentIndex={tabIndex}>This page intentionally left blank.</TabPanel>
      </main>
    </div>
  );
}

export default App;
